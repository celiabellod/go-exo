package certificat

import (
	"testing"
	"time"
)

func TestValidCertificatData(t *testing.T) {
	certificat, err := New("Golang", "Bob", "2018-05-02")
	if err != nil {
		t.Errorf("Certificat data should be valid. err=%v", err)
	}
	if certificat == nil {
		t.Errorf("Certificat should be a valid reference. got=nil")
	}
	if certificat.Course != "GOLANG COURSE" {
		t.Errorf("Course name is not valid. Expected ='GOLANG COURSE', got=%v", certificat.Course)
	}
	if certificat.Name != "BOB" {
		t.Errorf("Name is not valid. Expected ='BOB', got=%v", certificat.Name)
	}
}

func TestCourseEmptyValue(t *testing.T) {
	_, err := New("", "Bob", "2018-05-02")
	if err == nil {
		t.Errorf("Error should be returned on an empty course")
	}
}

func TestCourseTooLong(t *testing.T) {
	course := "fjedksghujgdbhjhfdshjdzshjknsdhjksxjknsdjefdjdesjxhjnbjnkbh"
	_, err := New(course, "Bob", "2018-05-02")
	if err == nil {
		t.Errorf("Error should be returned on a too long course name (course=%s)", course)
	}
}

func TestNameEmptyValue(t *testing.T) {
	_, err := New("Golang", "", "2018-05-02")
	if err == nil {
		t.Errorf("Error should be returned on an empty name")
	}
}
func TestNameTooLong(t *testing.T) {
	name := "fjedksghujgdbhjhfdshjdzshjknsdhjksxjknsdjefdjdesjxhjnbjnkbdesrh"
	_, err := New("Golang", name, "2018-05-02")
	if err == nil {
		t.Errorf("Error should be returned on a too long name (name=%s)", name)
	}
}

func TestDateIsParse(t *testing.T) {
	date := "2018-05-02"
	certificat, _ := New("Golang", "Bob", date)

	parse_date, _ := time.Parse("2006-01-02", date)
	if certificat.Date != parse_date {
		t.Errorf("Certificat date should be parsed. got=%v", certificat.Date)
	}
}
