package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"strings"
)

func processLine(line, old, new string) (found bool, result string, occurence int) {
	result = line
	oldLower := strings.ToLower(old)
	lineLower := strings.ToLower(line)
	found = strings.Contains(lineLower, oldLower)

	if !found {
		return 
	}

	occurence = strings.Count(lineLower, oldLower)
	result = strings.Replace(lineLower, oldLower, new, -1)
	return
}

func FindReplaceFile(oldSrc, newSrc, old, new string) (occurence int, lines []int, err error){
	oldFile, err := os.Open(oldSrc)
	if err != nil {
		return 0, nil, fmt.Errorf("The file %s doesnt exist", oldSrc)
	}
	defer oldFile.Close()

	newFile, err := os.Create(newSrc)
	if err != nil {
		return occurence, lines, err
	}
	defer newFile.Close()

	scanner := bufio.NewScanner(oldFile)

	writer := bufio.NewWriter(newFile)
	defer writer.Flush()

	var lines_number int = 1

	for scanner.Scan() {
		_, result, occurenceWord := processLine(scanner.Text(), old, new)

		occurence += occurenceWord
		lines = append(lines, lines_number)

		_, err := fmt.Fprintln(writer, result)
		if err != nil {
			return occurence, lines, err
		}
		

		lines_number += 1
	}

	if err := scanner.Err(); err != nil {
		return 0, nil, fmt.Errorf("The file %s can't be reading", oldSrc)
	}

	return occurence, lines, nil
}

func main() {
	oldSrc := flag.String("oldSrc", "", "oldSrc")
	newSrc := flag.String("newSrc", "", "newSrc")
	oldValue := flag.String("oldValue", "", "oldValue")
	newValue := flag.String("newValue", "", "newValue")
	flag.Parse()

	occurence, lines, err := FindReplaceFile(*oldSrc, *newSrc, *oldValue, *newValue)
	if err != nil {
		fmt.Printf("An error is occured: %v\n", err)
	} else {
		fmt.Printf("== Summary ==\n")
		defer fmt.Printf("== End of Summary ==\n")

		fmt.Printf("Number of occurences of %s: %d\n", *oldValue, occurence)
		fmt.Printf("Number of lines: %d\n", len(lines))
		fmt.Printf("Lines: %v\n", lines)
	} 
}