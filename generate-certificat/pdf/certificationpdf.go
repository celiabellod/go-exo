package pdf

import (
	"fmt"
	"generate-certificat/certificat"
	"os"
	"path"

	"github.com/jung-kurt/gofpdf"
)

type PdfSaver struct {
	OutputDir string
}

func New(outputDir string) (*PdfSaver, error) {
	var p *PdfSaver
	err := os.MkdirAll(outputDir, os.ModePerm)
	if err != nil {
		return p, nil
	}

	p = &PdfSaver{
		OutputDir: outputDir,
	}

	return p, nil
}

func (p *PdfSaver) Save(certificat certificat.Certificat) error {
	pdf := gofpdf.New(gofpdf.OrientationLandscape, "mm", "A4", "")

	pdf.SetTitle(certificat.LabelTitle, false)
	pdf.AddPage()

	background(pdf)

	header(pdf, &certificat)

	body(pdf, &certificat)

	footer(pdf)

	filename := fmt.Sprintf("%v.pdf", certificat.LabelTitle)
	path := path.Join(p.OutputDir, filename)
	err := pdf.OutputFileAndClose(path)
	if err != nil {
		return err
	}

	fmt.Printf("Saved certificate to '%v'\n", path)
	return nil
}

func background(pdf *gofpdf.Fpdf) {
	opts := gofpdf.ImageOptions{
		ImageType: "png",
	}
	pageWidth, pageHeight := pdf.GetPageSize()
	pdf.ImageOptions(
		"img/background.png",
		0, 0,
		pageWidth, pageHeight,
		false, opts, 0, "",
	)
}

func header(pdf *gofpdf.Fpdf, certificat *certificat.Certificat) {
	opts := gofpdf.ImageOptions{
		ImageType: "png",
	}
	margin := 30.0
	x := 0.0
	imageWidth := 30.0
	filename := "img/gopher.png"
	pdf.ImageOptions(
		filename,
		x+margin, 20,
		imageWidth, 0,
		false, opts, 0, "",
	)

	pageWidth, _ := pdf.GetPageSize()
	x = pageWidth - imageWidth
	pdf.ImageOptions(
		filename,
		x-margin, 20,
		imageWidth, 0,
		false, opts, 0, "",
	)

	pdf.SetFont("Helvetica", "", 40)
	pdf.WriteAligned(0, 50, certificat.LabelCompletion, "C")

	pdf.Ln(30)
}

func body(pdf *gofpdf.Fpdf, certificat *certificat.Certificat) {
	pdf.SetFont("Helvetica", "I", 20)
	pdf.WriteAligned(0, 50, certificat.LabelPresented, "C")
	pdf.Ln(30)

	pdf.SetFont("Times", "B", 40)
	pdf.WriteAligned(0, 50, certificat.Name, "C")
	pdf.Ln(30)

	pdf.SetFont("Helvetica", "I", 20)
	pdf.WriteAligned(0, 50, certificat.LabelParticipation, "C")
	pdf.Ln(30)

	pdf.SetFont("Helvetica", "I", 15)
	pdf.WriteAligned(0, 50, certificat.LabelDate, "C")
}

func footer(pdf *gofpdf.Fpdf) {
	opts := gofpdf.ImageOptions{
		ImageType: "png",
	}

	imageWidth := 50.0
	pageWidth, pageHeight := pdf.GetPageSize()
	x := pageWidth - imageWidth - 20.0
	y := pageHeight - imageWidth - 10.0

	pdf.ImageOptions(
		"img/stamp.png",
		x, y,
		imageWidth, 0,
		false, opts, 0, "",
	)
}
