package certificat

import (
	"encoding/csv"
	"os"
)

func ParseCSV(filename string) ([]*Certificat, error) {
	certificats := make([]*Certificat, 0)
	f, err := os.Open(filename)
	if err != nil {
		return certificats, err
	}
	defer f.Close()

	r := csv.NewReader(f)
	records, err := r.ReadAll()
	if err != nil {
		return certificats, err
	}

	for _, rec := range records {
		course := rec[0]
		name := rec[1]
		date := rec[2]

		c, err := New(course, name, date)

		if err != nil {
			return certificats, err
		}

		certificats = append(certificats, c)
	}

	return certificats, nil
}
