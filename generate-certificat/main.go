package main

import (
	"flag"
	"fmt"
	"generate-certificat/certificat"
	"generate-certificat/html"
	"generate-certificat/pdf"
	"os"
)

func main() {
	outputType := flag.String("type", "pdf", "Output type of the certificate.")
	file := flag.String("file", "", "CSV file input.")
	flag.Parse()

	if len(*file) <= 0 {
		fmt.Printf("Invalid file. got '%v'\n", *file)
		os.Exit(1)
	}
	var saver certificat.Saver
	var err error

	switch *outputType {
	case "html":
		saver, err = html.New("output")
	case "pdf":
		saver, err = pdf.New("output")
	default:
		fmt.Printf("Unknow output type. got '%v'\n", *outputType)
	}

	if err != nil {
		fmt.Printf("Could not create generator: %v", err)
		os.Exit(1)
	}

	certificats, err := certificat.ParseCSV(*file)
	if err != nil {
		fmt.Printf("Could not parse CSV file: %v", err)
		os.Exit(1)
	}

	for _, c := range certificats {
		saver.Save(*c)
	}
	if err != nil {
		fmt.Printf("Could not save Certificat. got=%v\n", err)
	}
}
